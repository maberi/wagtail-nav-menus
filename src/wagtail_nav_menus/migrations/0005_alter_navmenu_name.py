# Generated by Django 3.2.13 on 2023-03-31 11:26

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('wagtail_nav_menus', '0004_alter_navmenu_id'),
    ]

    operations = [
        migrations.AlterField(
            model_name='navmenu',
            name='name',
            field=models.CharField(choices=[('top', 'Cabecera'), ('footer', 'Pie')], max_length=50),
        ),
    ]
